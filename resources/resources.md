# Resources

The following resources are available within the CB-Geo group.

* [![appear.in](images/appear.in.png) appear.in](https://appear.in/cb-geo): Video conferencing

* [![asana](images/asana.png) Asana](https://asana.com/):  Project management

* [![docker](images/docker.png) Docker](https://hub.docker.com/u/cbgeo): Lightweight container

* [![github](images/github.png) GitHub](https://github.com/cb-geo): Git code hosting

* [![gitlab](images/gitlab.png) GitLab](https://gitlab.com/groups/cb-geo): Git code hosting (mirror of GitHub)

* [![slack](images/slack.png) Slack](https://cb-geo.slack.com/): Team communication

* [![slack](images/travis.png) Travis-CI](https://travis-ci.org/cb-geo): Continous integration

* [![vagrant](images/vagrant.png) Vagrant](https://www.vagrantup.com/): Virtual environment deployment
