# Summary

* [Introduction](README.md)
* [Resources](resources/resources.md)
* [Git](git/git.md)
  * [CB-Geo Git guidelines](git/guidelines.md)
  * [Mirroring Git repos](git/mirror.md)
* [Docker](docker/docker.md)
* [Vagrant](vagrant/vagrant.md)
